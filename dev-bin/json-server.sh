
if [ -z "$1" ]
then
    ./node_modules/.bin/json-server -H 0.0.0.0 -d 1500 -w db.json
else
    ./node_modules/.bin/json-server -H 0.0.0.0 $@
fi
